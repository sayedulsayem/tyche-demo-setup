<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'e-commerce' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'f0o7:}?2OyfX61(?`5h{%*>kOb7T,+q1p(GyC~RX1InQb 5&3?v9&}UK7e8ZL!Ok' );
define( 'SECURE_AUTH_KEY',  ';3T6&sQ=I^L~t*+JG`hZW3|GbXm$FI8=?H7jypz~4BcJY>?z&UsE.(_Ry}+`g;^B' );
define( 'LOGGED_IN_KEY',    '^[$O<qt6eD.Ot.81]c)wfi{[JE7dg:?$`JXP@bXphaiX,,]Y;In7ua!Dvp&$%p@8' );
define( 'NONCE_KEY',        'HTFCp</k,M!Y$Gs_JYFcM}`t6,/<(FZafF<u)pNf%,O>DRW*)3|D?}4?y}x$H7Vd' );
define( 'AUTH_SALT',        'Yj6;3y~/E7MSn|)-3)[)HQxatHJcUF;mWka#v:2U(Za sgJcqzxL_y}&&HgkWu3[' );
define( 'SECURE_AUTH_SALT', ';O<+o-,_;*i.nuSbfJr:O;U(ww-MuRm(.*8A &Mj5*z/4a<w!#OL,S~;NC8qTh8N' );
define( 'LOGGED_IN_SALT',   '3@18:XG#a!Ao6i5$[t%o+@z&s-h@r8z@Cu G<>g^[.~,w.<*+sFuPp}V}GerFxA]' );
define( 'NONCE_SALT',       'iYV{~4a9dN|9^$c2X^S=ya2Glj.U3!iFS86$%f:>0.bdnF4Bvu@NS.9trO2#9=G9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
